const RANGE_MAX_VALUE  = 30;

$(e=>{
    init_ranges();
    init_circles();
    init_sliders();
    const swiper = new Swiper('.ak-suggestion-carousel .swiper', {
      
        // Navigation arrows
        navigation: {
          nextEl: '.swiper-button-prev',
          prevEl: '.swiper-button-next',
        },
        rtl: true,
        slidesPerView: 1,
        spaceBetween: 10,
        // Responsive breakpoints
        breakpoints: {
            320: {
            slidesPerView: 2,
            spaceBetween: 20
            },
            480: {
            slidesPerView: 3,
            spaceBetween: 20
            },
            640: {
            slidesPerView: 7,
            spaceBetween: 20
            }
        }
      
    });
    const swiper2 = new Swiper('.ak-history-carousel .swiper', {
      
        // Navigation arrows
        // navigation: {
        //   nextEl: '.swiper-button-prev',
        //   prevEl: '.swiper-button-next',
        // },
        rtl: true,
        slidesPerView: 1,
        spaceBetween: 10,
        // Responsive breakpoints
        breakpoints: {
            320: {
            slidesPerView: 2,
            spaceBetween: 20
            },
            480: {
            slidesPerView: 3,
            spaceBetween: 30
            },
            640: {
            slidesPerView: 6,
            spaceBetween: 200
            },
            992: {
                slidesPerView: 8,
                spaceBetween: 20
                }
        }
      
    });
    const swiper3 = new Swiper('.ak-xp-sliders .swiper', {
      
        // Navigation arrows
        navigation: {
          nextEl: '.ak-xp-sliders .swiper-button-next',
          prevEl: '.ak-xp-sliders .swiper-button-prev',
        },
        rtl: false,
        slidesPerView: 1,
        spaceBetween: 10,
        initialSlide: 1000,
        // Responsive breakpoints
        breakpoints: {
            320: {
                slidesPerView: 1,
                spaceBetween: 20
            },
            480: {
                slidesPerView: 1.5,
                spaceBetween: 30
            },
            640: {
                slidesPerView: 3,
                spaceBetween: 40
            },
            992: {
                slidesPerView: 4,
                spaceBetween: 40
            }
        }
      
    });
    $(".rateyo").each((i, item) =>{
        $(item).rateYo({
            starWidth: "20px",
            ratedFill: "#FFA41C",
            normalFill: "#e1e1e1",
            readOnly: true,
            rating: $(item).data("rate"),
        });
    });
      
});

$.widget('akbarbey.akRange', {
    options:{
        maxValue: RANGE_MAX_VALUE ,
        value: 0,
    },
    width: 0, 
    _create: function () {
        const self = this;
        let width = self.options.value / self.options.maxValue * 100;
        $(self.element).find(".range-fill").css("width", width+"%" );
        
        $(self.element).find("b.level").text(self.options.value);
    }
});

$.widget('akbarbey.point', {
    options:{
        maxValue: RANGE_MAX_VALUE ,
        level: 0,
        xp:0,
        maxXp: 30,
        k: true,
        lvl: undefined
    },
    width: 0, 
    _create: function () {
        const self = this;
        let width = self.options.xp / self.options.maxXp * 100;
        $(self.element).find(".range-fill").css("width", width+"%" );
        let xp = self.options.k && self.options.xp/1000  || self.options.xp;
        let mxp = self.options.k && self.options.maxXp/1000  || self.options.maxXp;
        $(self.element).find(".xp-value").text(xp);
        $(self.element).find(".xp-value-max").text(mxp);
        $(self.element).find(".ak-point-level span").text(self.options.level);
        if (self.options.lvl){
            $(self.element).parent().find(".ak-level .xp-value").text(xp/1000);
            $(self.element).parent().find(".ak-level .xp-value-max").text(mxp/1000);
            $(self.element).parent().find(".range-fill").css("width", width+"%" );
            $(self.element).parent().find(".ak-point-level span").text(self.options.level);
        }
        $(self.element).find("b.level").text(self.options.value);
    }
});


function init_ranges() {
    $(".ak-point").each((i, item)=>{
        $(item).point({
            level: $(item).data("level"),
            xp: $(item).data("xp"),
            maxXp: $(item).data("max-xp"),
            k: $(item).data("k"),
            lvl:$(item).data("lvl"),
        })
    });
}
function init_circles() {
    $(".ak-progress").each((i, item)=>{
        let width = $(item).parent().hasClass("ak-point-circle-lg") && 5  || 3;
        let trailWidth = width;
        let color = $(item).data("color") || "#FFB923";
        let trailColor = $(item).data("trail-color") ||  '#eee';
        var bar = new ProgressBar.Circle(item, {
            progress: $(item).data("progress"),
            color: color,
            trailColor: trailColor,
            trailWidth:trailWidth,
            duration: 1400,
            easing: 'bounce',
            strokeWidth: width,
            from: {color: color, a:0},
            to: {color: color, a:1},
            // Set default step function for all animate calls
            step: function(state, circle) {
            circle.path.setAttribute('stroke', state.color);
            }
        });
        let p = $(bar.element)    
        $(item).siblings(".ak-point").find(".ak-xp-color").css("color", bar._opts.color);
        bar.animate(bar._opts.progress || 0.5);  // Number from 0.0 to 1.0
    });
}
const range = [-10,-9, -8,-7,-6,-5,-4,-3,-2,-1,0,1,2,3,4,5,6,7,8,9,10];
function init_sliders() {
    $(".ak-slider-widget").each((i, item)=>{
        $(item)
        .slider({
            min:-10,
            max: 10,
            value: 0,
            onchange: function (e) {
                let x = 0;
                $(".ui-slider-pip-selected .ui-slider-label").each((i, item)=>{
                    x += $(item).data("value");
                });
                $(".ak-new-review-xp b").text(x > 0 && "+"+x || x)
            }
            
        })
        .slider("pips", {
            suffix: " xp",
            
        });
    });
}